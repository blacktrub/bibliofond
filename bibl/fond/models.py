from django.db import models
from django.db.models import Q

from .utils import to_choices
from office.models import Client


class Book(models.Model):
    class TYPE:
        BOOK = 0
        JOURNAL = 1
        ARTICLE = 2

    author = models.CharField(max_length=128, null=True, blank=True)
    name = models.CharField(max_length=128, null=True, blank=True)
    genre = models.CharField(max_length=128, null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)

    type = models.IntegerField(choices=to_choices(TYPE), default=TYPE.BOOK, null=True, blank=True)
    pages = models.IntegerField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    fond = models.ForeignKey('Fond', null=True, blank=True)
    is_busy = models.BooleanField(default=False)

    @property
    def status(self):
        verbose_status = {
            True: 'Занята',
            False: 'Свободна'
        }
        return verbose_status[self.is_busy]

    @property
    def display_type(self):
        verbose_types = {
            self.TYPE.BOOK: 'Книга',
            self.TYPE.JOURNAL: 'Журнал',
            self.TYPE.ARTICLE: 'Статья',
        }

        return verbose_types[self.type]

    @classmethod
    def search(cls, query):
        books = cls.objects.filter(
            Q(author__contains=query) | Q(name__contains=query)
        ).filter(is_busy=False)
        return books

    def to_reserve(self):
        reserve_fond, _ = Fond.objects.get_or_create(name='Резерв', type=Fond.TYPE.RESERVE)
        self.fond = reserve_fond
        self.save()

    def to_base(self):
        base_fond, _ = Fond.objects.get_or_create(type=Fond.TYPE.BASE)
        self.fond = base_fond
        self.save()


class Fond(models.Model):
    class TYPE:
        BASE = 0
        RESERVE = 1
        EXCHANGE = 2

    type = models.IntegerField(choices=to_choices(TYPE), default=TYPE.BASE)
    name = models.CharField(default='Основной', max_length=128)

    @property
    def is_base(self):
        return self.type == self.TYPE.BASE

    @property
    def is_reserve(self):
        return self.type == self.TYPE.RESERVE


class History(models.Model):
    book = models.ForeignKey(Book)
    client = models.ForeignKey(Client)

    created = models.DateTimeField(auto_now_add=True)
