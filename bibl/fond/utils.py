def to_choices(_class):
    result = []
    for attr in dir(_class):
        if attr.startswith('__'):
            continue
        result.append((getattr(_class, attr), attr))
    return result
