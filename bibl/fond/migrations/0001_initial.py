# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-11 13:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(max_length=128)),
                ('name', models.CharField(max_length=128)),
                ('genre', models.CharField(max_length=128)),
                ('year', models.IntegerField()),
                ('type', models.IntegerField(choices=[(2, 'ARTICLE'), (0, 'BOOK'), (1, 'JOURNAL')], default=0)),
                ('pages', models.IntegerField()),
                ('description', models.TextField()),
                ('is_busy', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Fond',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.IntegerField(choices=[(0, 'BASE'), (1, 'EXCHANGE'), (2, 'REVERSE')], default=0)),
                ('name', models.CharField(default='Основной', max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='History',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fond.Book')),
            ],
        ),
    ]
