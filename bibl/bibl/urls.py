"""bibl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from office import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index_page, name='index'),
    url(r'^book/(?P<book_id>\d+)/$', views.book_detail_page, name='detail_book'),
    url(r'^book/(?P<book_id>\d+)/to/reserve/$', views.book_to_reserve, name='book_to_reserve'),
    url(r'^book/(?P<book_id>\d+)/to/base/$', views.book_to_base, name='book_to_base'),
    url(r'^book/(?P<book_id>\d+)/give/$', views.give_book_page, name='give_book_page'),
    url(r'^book/(?P<book_id>\d+)/return/$', views.return_book, name='return_book'),
    url(r'^book/create/$', views.create_book_page, name='create_book_page'),
    url(r'^client/$', views.client_page, name='client_page'),
    url(r'^fond/stats/$', views.fonds_stats_page, name='fond_stats_page'),
]
