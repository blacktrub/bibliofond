from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager


class Client(AbstractUser):
    taken_book = models.BooleanField(default=False)

    objects = UserManager()

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'

    def take_book(self, book):
        from fond.models import History
        History.objects.create(client=self, book=book)

        book.is_busy = True
        book.save()

        self.taken_book = True
        self.save()

    def return_book(self, book):
        book.is_busy = False
        book.save()

        self.taken_book = False
        self.save()
