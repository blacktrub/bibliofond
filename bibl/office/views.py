from django.shortcuts import render, redirect

from fond.models import Book, Fond, History
from office.models import Client


def index_page(request):
    context = {}

    if request.method == 'POST':
        query = request.POST.get('query')
        print(query)

        if query:
            books = Book.search(query)
            context['books'] = books

    return render(request, 'index.html', context=context)


def book_detail_page(request, book_id):
    book = Book.objects.get(id=book_id)
    return render(request, 'book_detail.html', {'book': book})


def book_to_reserve(request, book_id):
    book = Book.objects.get(id=book_id)
    book.to_reserve()
    return redirect('detail_book', book_id=book.id)


def book_to_base(request, book_id):
    book = Book.objects.get(id=book_id)
    book.to_base()
    return redirect('detail_book', book_id=book.id)


def give_book_page(request, book_id):
    context = {}
    book = Book.objects.get(id=book_id)
    clients = Client.objects.exclude(is_staff=True)

    if request.method == 'POST':
        client_id = request.POST.get('client_id')
        client = Client.objects.get(id=client_id)
        context['client'] = client

        if client.taken_book:
            context['error'] = True
        else:
            client.take_book(book)
            context['success'] = True

    context['book'] = book
    context['clients'] = clients
    return render(request, 'give_book.html', context=context)


def return_book(request, book_id):
    book = Book.objects.get(id=book_id)
    history = History.objects.filter(book=book).order_by('created').last()

    client = history.client
    client.return_book(book)
    return redirect('detail_book', book_id=book_id)


def client_page(request):
    context = {}

    if request.method == 'POST':
        email = request.POST.get('email')
        name = request.POST.get('name')
        client, created = Client.objects.get_or_create(email=email, first_name=name, username=email)

        if created:
            context['success'] = True
            context['name'] = name

    return render(request, 'new_client.html', context=context)


def fonds_stats_page(request):
    context = {}
    fonds = Fond.objects.all()
    books = Book.objects.all()

    context['fonds'] = {}
    for fond in fonds:
        context['fonds'][fond] = books.filter(fond=fond)

    return render(request, 'fonds_stats.html', context=context)


def create_book_page(request):
    context = {}

    all_fonds = Fond.objects.all()
    types_book = {
        'Книга': 0,
        'Журнал': 1,
        'Статья': 2,
    }

    context['fonds'] = all_fonds
    context['types'] = types_book

    if request.method == 'POST':
        author = request.POST.get('author')
        name = request.POST.get('name')
        genre = request.POST.get('genre')
        year = request.POST.get('year')
        type_book = request.POST.get('type')
        pages = request.POST.get('pages')
        description = request.POST.get('description')
        fond = request.POST.get('fond')

        data = {
            'author': author,
            'name': name,
            'genre': genre,
            'year': year,
            'type': type_book,
            'pages': pages,
            'description': description,
            'fond_id': fond,
        }

        book, created = Book.objects.get_or_create(**data)
        return redirect('detail_book', book_id=book.id)

    return render(request, 'create_book.html', context=context)
